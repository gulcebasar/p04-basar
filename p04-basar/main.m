//
//  main.m
//  p04-basar
//
//  Created by Gulce on 19/02/16.
//  Copyright © 2016 Gulce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
