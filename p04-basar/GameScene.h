//
//  GameScene.h
//  p04-basar
//

//  Copyright (c) 2016 Gulce. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@property (strong) CMMotionManager* motionManager;

@property SKLabelNode *myLabel;
@property NSInteger score;
@property (nonatomic, strong) NSTimer *timer;

@property (strong) SKNode* spaceship;
@property (strong) SKNode* wrapAroundShip;
@property (strong) NSMutableArray *Asteroids;

// Buttons
@property (strong) SKLabelNode* fireButton;
@property (strong) SKLabelNode* rotateLeftButton;
@property (strong) SKLabelNode* rotateRightButton;
@property (strong) SKLabelNode *restartButton;

// Button functions
-(void)fireBullet;
-(void)createFireButton;
-(void)createRotateButtons;


// Ship functions
-(void)rotateShipLeft;
-(void)rotateShipRight;


@end
