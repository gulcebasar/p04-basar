//
//  AppDelegate.h
//  p04-basar
//
//  Created by Gulce on 19/02/16.
//  Copyright © 2016 Gulce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

