//
//  GameScene.m
//  p04-basar
//
//  Created by Gulce on 19/02/16.
//  Copyright (c) 2016 Gulce. All rights reserved.
//

#import "GameScene.h"

// Collision bits
NS_ENUM(uint32_t, AstPhysicsCategories) {
    AstPhysicsCategoryShip      = 1 << 0,
    AstPhysicsCategoryBullet    = 1 << 1,
    AstPhysicsCategoryAsteroid  = 1 << 2,
    AstPhysicsCategoryWalls     = 1 << 3,
};

// Ship rotation amount
double shipRotation = 0;

//value to increase asteroid creation
NSInteger asteroidIncrease = 0;
float timeInterval = 0;

@implementation GameScene

-(void)createRotateButtons
{
    NSLog(@"Added rotate buttons");
    self.rotateLeftButton = [SKLabelNode labelNodeWithText:@"↪️"];
    self.rotateLeftButton.fontSize = 52;
    self.rotateLeftButton.position = CGPointMake(30, 20);
    [self addChild:self.rotateLeftButton];
    
    self.rotateRightButton = [SKLabelNode labelNodeWithText:@"↩️"];
    self.rotateRightButton.fontSize = 52;
    self.rotateRightButton.position = CGPointMake(80, 20);
    [self addChild:self.rotateRightButton];
}

-(void)createFireButton
{
    NSLog(@"Added firebutton");
    self.fireButton = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
//    self.fireButton.text = @"Fire! 🔥";
    self.fireButton.text = @"🔥";
    self.fireButton.fontSize = 52;
    self.fireButton.fontColor = [SKColor greenColor];
    self.fireButton.position = CGPointMake(self.size.width - 40, 20);
    [self addChild:self.fireButton];
}

-(void)createRestartButton
{
    NSLog(@"Added rotate buttons");
    self.restartButton = [SKLabelNode labelNodeWithText:@"Restart"];
    self.restartButton.name = @"Restart";
    self.restartButton.fontSize = 25;
    self.restartButton.fontName = @"ChalkboardSE-Bold";
    self.restartButton.position = CGPointMake(self.size.width / 2.0f, (self.size.height / 2.0f)-40);
    self.restartButton.fontColor = [UIColor grayColor];
    [self addChild:self.restartButton];
}

-(void)createSpaceShip
{
    self.wrapAroundShip = nil;
    self.spaceship = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
    self.spaceship.name = @"SpaceShip";
    
    self.spaceship.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.spaceship.frame.size];
    self.spaceship.physicsBody.categoryBitMask = AstPhysicsCategoryShip;
    self.spaceship.physicsBody.collisionBitMask = AstPhysicsCategoryAsteroid;
    self.spaceship.physicsBody.contactTestBitMask = AstPhysicsCategoryWalls | AstPhysicsCategoryAsteroid;
    self.spaceship.physicsBody.dynamic = YES;
    self.spaceship.physicsBody.affectedByGravity = NO;
    
    self.spaceship.position = CGPointMake(self.size.width / 2.0f, self.size.height / 2.0f);
    [self addChild:self.spaceship];
}

-(void)createAsteroid
{
    SKNode* asteroid;
    if(arc4random()%5== 0)
    {
        asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid1"];
        asteroid.name = @"bigAsteroid";
    }
    else if(arc4random()%2 == 0)
    {
        asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid2"];
        asteroid.name = @"smallAsteroid";
    }
    else
    {
        asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid3"];
        asteroid.name = @"smallAsteroid";
    }
    
    asteroid.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:asteroid.frame.size];
    asteroid.physicsBody.categoryBitMask = AstPhysicsCategoryAsteroid;
    asteroid.physicsBody.collisionBitMask = AstPhysicsCategoryBullet | AstPhysicsCategoryShip;
    asteroid.physicsBody.contactTestBitMask = AstPhysicsCategoryWalls ;
    asteroid.physicsBody.dynamic = YES;
    asteroid.physicsBody.affectedByGravity = NO;
    asteroid.userData = [NSMutableDictionary dictionary];
    asteroid.zRotation +=  M_PI/(float)(arc4random()%10);
    
    int height=self.size.height;
    int width=self.size.width;
    
    int xVel = 0;
    int yVel = 0;
    
    int location = arc4random()%4;
    if( location == 0 ) //bottom
    {
        NSLog(@"Asteroid appeared on bottom");
        asteroid.position = CGPointMake(arc4random()%width, 0);
        xVel = (arc4random()% 3) - (arc4random()% 3);
        yVel = (arc4random()% 3);
    }
    else if( location == 1 ) //top
    {
        NSLog(@"Asteroid appeared on top");
        asteroid.position = CGPointMake(arc4random()%width, height);
        xVel = (arc4random()% 3) - (arc4random()% 3);
        yVel = -(arc4random()% 3);
    }
    else if( location == 2 ) //right
    {
        NSLog(@"Asteroid appeared on right");
        asteroid.position = CGPointMake(width, arc4random()%height);
        xVel = -(arc4random()% 3);
        yVel = (arc4random()% 3) - (arc4random()% 3);
    }
    else if( location == 3 ) //left
    {
        NSLog(@"Asteroid appeared on left");
        asteroid.position = CGPointMake(0, arc4random()%height);
        xVel = (arc4random()% 3);
        yVel = (arc4random()% 3) - (arc4random()% 3);
    }
    
    if (xVel == 0 && yVel == 0) xVel = 1;

    asteroid.userData[@"xVel"] = [NSValue valueWithBytes:&xVel objCType:@encode(int)];
    asteroid.userData[@"yVel"] = [NSValue valueWithBytes:&yVel objCType:@encode(int)];
    
    [self.Asteroids addObject:asteroid];
    [self addChild:asteroid];
}

-(void)timerEvent:(id)sender
{
    [self createAsteroid];
}

-(void)didMoveToView:(SKView *)view
{
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.name = @"Background";
    background.size = self.frame.size;
    [self addChild:background];
    
    self.Asteroids = [[NSMutableArray alloc] init];
    self.score = 0;
    self.myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    self.myLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.score];
    self.myLabel.fontSize = 15;
    self.myLabel.position = CGPointMake(10 + (self.myLabel.frame.size.width/2) , self.size.height - 25);
    [self addChild:self.myLabel];

    asteroidIncrease = 30;
    timeInterval = 5;
    
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = AstPhysicsCategoryWalls;
    
    [self createSpaceShip];
    [self createFireButton];
    [self createRotateButtons];
    
    self.motionManager = [[CMMotionManager alloc] init];
    [self.motionManager startAccelerometerUpdates];
    self.physicsWorld.contactDelegate = self;
    
    //creates asteroid every 5 seconds
    self.timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

-(void)userMotion:(NSTimeInterval)time
{
    SKSpriteNode* spaceShip = (SKSpriteNode*)[self childNodeWithName:@"SpaceShip"];
    CMAccelerometerData* data = self.motionManager.accelerometerData;
    
    CGPoint pos = spaceShip.position;
    if (fabs(data.acceleration.y) > 0.1) {
        pos.x += 5 * data.acceleration.y;
    }
    if (fabs(data.acceleration.x) > 0.1) {
        pos.y += 5 * -data.acceleration.x;
    }
    spaceShip.position = pos;
    if (shipRotation != 0) {
        [self.spaceship runAction:[SKAction rotateByAngle:shipRotation duration:0.02f]];
    }
}

-(void)moveAsteroids
{
    for( SKNode* asteroid in self.Asteroids )
    {
        int xLoc = 0, yLoc = 0;
        NSValue *xVal = asteroid.userData[@"xVel"];
        if (xVal)
            [xVal getValue:&xLoc];
        
        NSValue *yVal = asteroid.userData[@"yVel"];
        if (yVal)
            [yVal getValue:&yLoc];
        
        CGPoint pos2 = asteroid.position;
        
        pos2.x += xLoc;
        pos2.y += yLoc;
        asteroid.position = pos2;
    }
}

-(void)fireBullet
{
    SKSpriteNode* spaceShip = (SKSpriteNode*)[self childNodeWithName:@"SpaceShip"];

    CGFloat rotation = self.spaceship.zRotation + M_PI_2;
    CGPoint bulletPos = CGPointMake(spaceShip.position.x + (spaceShip.size.width / 2.5f) *cos(rotation), spaceShip.position.y + (spaceShip.size.height / 2.5f) * sin(rotation));
    
    SKLabelNode* bullet = [SKLabelNode labelNodeWithText:@"🔥"];
    bullet.fontSize = 12;
    bullet.zRotation = self.spaceship.zRotation;
    bullet.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bullet.frame.size];
    bullet.physicsBody.categoryBitMask = AstPhysicsCategoryBullet;
    bullet.physicsBody.collisionBitMask = AstPhysicsCategoryAsteroid;
    bullet.physicsBody.contactTestBitMask = AstPhysicsCategoryWalls | AstPhysicsCategoryAsteroid;
    bullet.physicsBody.dynamic = YES;
    bullet.physicsBody.affectedByGravity = NO;
    bullet.physicsBody.restitution = 1.0;
    bullet.physicsBody.friction = 0.0;
    bullet.physicsBody.linearDamping = 0.0;
    bullet.physicsBody.angularDamping = 0.0;
    bullet.position = bulletPos;
    [self addChild:bullet];
    
    GLKVector2 velocity = GLKVector2Make(cos(rotation), sin(rotation));
    GLKVector2 direction = velocity;
    GLKVector2 newVelocity = GLKVector2MultiplyScalar(direction, 20 * 15);
    CGVector bulletVelocity = CGVectorMake(newVelocity.x, newVelocity.y);
    bullet.userData = [NSMutableDictionary dictionary];
    bullet.userData[@"bulletVelocity"] = [NSValue valueWithBytes:&bulletVelocity objCType:@encode(CGVector)];
    bullet.physicsBody.velocity = bulletVelocity;
    
    // Bullets fade out, remove from view when gone
    SKAction *action = [SKAction fadeOutWithDuration:0.8];
    action.timingMode = SKActionTimingEaseIn;
    [bullet runAction:action completion:^{
        [bullet removeFromParent];
    }];
}

-(void)rotateShipLeft
{
    NSLog(@"Rotate left");
    shipRotation = M_PI/56.0f;
}

-(void)rotateShipRight
{
    NSLog(@"Rotate right");
    shipRotation = -M_PI/56.0f;
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        // Allow user to hold rotate buttons, but not fire
        if (![self.fireButton containsPoint:location]) {
            if (![self.rotateLeftButton containsPoint:location] && ![self.rotateRightButton containsPoint:location]) {
                shipRotation = 0;
            }
        }
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        // Allow user to hold rotate buttons, but not fire
        if ([self.rotateLeftButton containsPoint:location]) {
            shipRotation = 0;
        }
        else if ([self.rotateRightButton containsPoint:location]) {
            shipRotation = 0;
        }
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if ( [self.fireButton containsPoint:location] && [self childNodeWithName:@"SpaceShip"] ) {
            NSLog(@"Firing bullet");
            [self fireBullet];
        }
        else if ([self.rotateLeftButton containsPoint:location]) {
            [self rotateShipLeft];
        }
        else if ([self.rotateRightButton containsPoint:location]) {
            [self rotateShipRight];
        }
        else if ([self.restartButton containsPoint:location] && [self childNodeWithName:@"Restart"]) {
            [ self removeAllChildren ];
            [ self didMoveToView:self.view ];
        }
    }
}

-(void)update:(CFTimeInterval)time
{
    /* Called before each frame is rendered */
    [self userMotion:time];
    [self moveAsteroids];
    
    if( self.score > asteroidIncrease )
    {
        asteroidIncrease += 20;
        [self.timer invalidate];
        
        timeInterval /= 2.0;
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"Contact");

    SKAction *action = [SKAction fadeOutWithDuration:0.2];
    action.timingMode = SKActionTimingEaseIn;
    
    // Spaceship hits asteroid
    if ( (contact.bodyA.node.physicsBody.categoryBitMask == AstPhysicsCategoryShip && contact.bodyB.node.physicsBody.categoryBitMask == AstPhysicsCategoryAsteroid)
        || (contact.bodyB.node.physicsBody.categoryBitMask == AstPhysicsCategoryShip && contact.bodyA.node.physicsBody.categoryBitMask == AstPhysicsCategoryAsteroid) )
    {
        self.spaceship.physicsBody = NULL;
        [self.spaceship runAction:action completion:^{
            [self.timer invalidate];
            [self.spaceship removeFromParent];
            for(SKNode* ast in self.Asteroids)
                [ast removeFromParent];
            [self.Asteroids removeAllObjects];
            //[self.fireButton removeFromParent];
            
            SKLabelNode* label = [SKLabelNode labelNodeWithText:@"Game Over!"];
            label.fontName = @"ChalkboardSE-Bold";
            label.fontSize = 60;
            label.position = CGPointMake(self.size.width / 2.0f, self.size.height / 2.0f);
            label.fontColor = [UIColor redColor];
            [self addChild:label];
            [self createRestartButton];
        }];
        
    }
    // Bullets hit asteroid
    if ( (contact.bodyA.node.physicsBody.categoryBitMask == AstPhysicsCategoryBullet && contact.bodyB.node.physicsBody.categoryBitMask == AstPhysicsCategoryAsteroid)
        || (contact.bodyB.node.physicsBody.categoryBitMask == AstPhysicsCategoryBullet && contact.bodyA.node.physicsBody.categoryBitMask == AstPhysicsCategoryAsteroid) )
    {
        SKNode *asteroid = (contact.bodyA.node.physicsBody.categoryBitMask == AstPhysicsCategoryAsteroid) ? contact.bodyA.node : contact.bodyB.node;
        
        contact.bodyA.node.physicsBody = NULL;
        contact.bodyB.node.physicsBody = NULL;
        
        [asteroid runAction:action completion:^{
            if([asteroid.name isEqualToString:@"bigAsteroid"])
            {
                //splits into 2
                self.score += 2;
                
                SKNode* asteroid1 = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid2"];
                SKNode* asteroid2 = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid3"];
                
                asteroid1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:asteroid.frame.size];
                asteroid1.physicsBody.categoryBitMask = AstPhysicsCategoryAsteroid;
                asteroid1.physicsBody.collisionBitMask = AstPhysicsCategoryBullet | AstPhysicsCategoryShip;
                asteroid1.physicsBody.contactTestBitMask = AstPhysicsCategoryWalls ;
                asteroid1.physicsBody.dynamic = YES;
                asteroid1.physicsBody.affectedByGravity = NO;
                asteroid1.userData = [NSMutableDictionary dictionary];
                asteroid1.zRotation +=  M_PI/(float)(arc4random()%10);
                
                asteroid2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:asteroid.frame.size];
                asteroid2.physicsBody.categoryBitMask = AstPhysicsCategoryAsteroid;
                asteroid2.physicsBody.collisionBitMask = AstPhysicsCategoryBullet | AstPhysicsCategoryShip;
                asteroid2.physicsBody.contactTestBitMask = AstPhysicsCategoryWalls ;
                asteroid2.physicsBody.dynamic = YES;
                asteroid2.physicsBody.affectedByGravity = NO;
                asteroid2.userData = [NSMutableDictionary dictionary];
                asteroid2.zRotation +=  M_PI/(float)(arc4random()%10);
                
                asteroid1.position = CGPointMake(asteroid.position.x, asteroid.position.y);
                asteroid2.position = CGPointMake(asteroid.position.x, asteroid.position.y);
                
                int xLoc = 0, yLoc = 0;
                NSValue *xVal = asteroid.userData[@"xVel"];
                if (xVal)
                    [xVal getValue:&xLoc];
                
                NSValue *yVal = asteroid.userData[@"yVel"];
                if (yVal)
                    [yVal getValue:&yLoc];
                
                int xVel1 = xLoc +1;
                int yVel1 = yLoc +1;
                if(xVel1 == 0 && yVel1 == 0) xVel1 = -1;
                
                int xVel2 = xLoc -1;
                int yVel2 = yLoc -1;
                if(xVel2 == 0 && yVel2 == 0) xVel2 = 1;
                
                asteroid1.userData[@"xVel"] = [NSValue valueWithBytes:&xVel1 objCType:@encode(int)];
                asteroid1.userData[@"yVel"] = [NSValue valueWithBytes:&yVel1 objCType:@encode(int)];
                
                asteroid2.userData[@"xVel"] = [NSValue valueWithBytes:&xVel2 objCType:@encode(int)];
                asteroid2.userData[@"yVel"] = [NSValue valueWithBytes:&yVel2 objCType:@encode(int)];
                
                [self.Asteroids addObject:asteroid1];
                [self addChild:asteroid1];
                
                [self.Asteroids addObject:asteroid2];
                [self addChild:asteroid2];
            }
            else
                self.score += 4;
            
            [contact.bodyA.node removeFromParent];
            [contact.bodyB.node removeFromParent];
            [self.Asteroids removeObject:asteroid];
        }];
    }
    
    self.myLabel.text = [NSString stringWithFormat:@"Score: %ld", (long)self.score];
}

- (void) didEndContact:(SKPhysicsContact *)contact
{
    NSLog(@"Contact ended");
    // Spaceship
    SKNode *bodyToWrap = (contact.bodyA == self.physicsBody) ? contact.bodyB.node : contact.bodyA.node;

    BOOL wrapped = NO;
    // Wrap x direction
    CGPoint pos = CGPointMake(bodyToWrap.position.x, bodyToWrap.position.y);
    if (bodyToWrap.position.x < 0) {
        pos.x = self.frame.size.width;
        wrapped = YES;
    }
    else if (bodyToWrap.position.x > self.frame.size.width) {
        pos.x = 0;
        wrapped = YES;
    }
    // Wrap y direction
    if (bodyToWrap.position.y < 0) {
        pos.y = self.frame.size.height;
        wrapped = YES;
    }
    else if (bodyToWrap.position.y > self.frame.size.height) {
        pos.y = 0;
        wrapped = YES;
    }
    
    // Don't remove from parent if we don't need to
    if (!wrapped)
        return;
    
    [bodyToWrap removeFromParent];
    bodyToWrap.position = pos;
    [self addChild:bodyToWrap];
    
    if (bodyToWrap.userData) {
        NSValue *bulletVelocity = bodyToWrap.userData[@"bulletVelocity"];
        if (bulletVelocity) {
            CGVector velocityStruct;
            [bulletVelocity getValue:&velocityStruct];
            bodyToWrap.physicsBody.velocity = velocityStruct;
            NSLog(@"Set velocity to %@", NSStringFromCGVector(bodyToWrap.physicsBody.velocity));
        }
    }
}

@end
